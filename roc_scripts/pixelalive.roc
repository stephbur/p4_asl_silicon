init
start40

clk 3
sda 18 (CLK + 15)
ctr 3 (CLK + 0)
tin 8 (CLK + 5)
dselroc 4

pxardac #dacparam#
dac  253 b10_000  CtrlReg (high cal range, StopAcq, TriggerDisable)
dac  25  100 Vcal
wbc 100
flush

--- enable pixel ----------------------------

cold :
#trim#
flush

--- reset ROC -------------------------------
mdelay 20

pgstop
pgset 0 b001000   0  reset
pgsingle
mdelay 20

--- setup readout timing --------------------
pgset 0 b000000  15  pg_resr
pgset 1 b000100  107  pg_cal
pgset 2 b000010  16  pg_trg
pgset 3 b100001   0  pg_tok pg_sync

mdelay 500
vppa #coloff# #rowoff# #outfile#

--init
