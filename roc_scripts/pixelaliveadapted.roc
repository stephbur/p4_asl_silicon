init
start40

clk 3
sda 18 (CLK + 15)
ctr 3 (CLK + 0)
tin 8 (CLK + 5)
dselroc 4

pxardac /home/stephan/phd/projects/202012_vp_experiment2/pyxar_mini/params.dac
dac  253 b10_000  CtrlReg (high cal range, StopAcq, TriggerDisable)
dac  25  100 Vcal
wbc 100
flush

--- enable pixel ----------------------------

cold :
pixe 20 35 12
pixe 21 35 10
pixe 22 35 7
pixe 23 35 10
pixe 20 36 15
pixe 21 36 11
pixe 22 36 2
pixe 23 36 6
pixe 20 37 12
pixe 21 37 0
pixe 22 37 10
pixe 23 37 12
pixe 20 38 10
pixe 21 38 14
pixe 22 38 12
pixe 23 38 15

flush

--- reset ROC -------------------------------
mdelay 20

pgstop
pgset 0 b001000   0  reset
pgsingle
mdelay 20

--- setup readout timing --------------------
pgset 0 b000000  15  pg_resr
pgset 1 b000100  107  pg_cal
pgset 2 b000010  16  pg_trg
pgset 3 b100001   0  pg_tok pg_sync

mdelay 500
vppa 20 35 /home/stephan/phd/projects/202012_vp_experiment2/pyxar_mini/data/pixelalive.csv

--init
