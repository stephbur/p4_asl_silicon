import os
import argparse
import numpy as np
import pandas as pd

COL_OFFSET = 20
ROW_OFFSET = 35

class pyxar_mini_cmd():

    def __init__(self, args):
        self.data_dir = os.path.abspath("data")
        self.trimfile = args.trim
        self.dacfile = args.dac
        self.resultfile = args.result
        

    def threshold_scan(self, trims, ):
        replacement_settings = [("dacparam", os.path.abspath("params.dac")),
                                ("coloff", COL_OFFSET),
                                ("rowoff", ROW_OFFSET),
                                ("outfile", os.path.join(self.data_dir, self.resultfile)),
                                ("trim", self.generate_trim_string())]
        self.adapt_and_run_script("roc_scripts/thrmap.roc", replacement_settings)
        df = pd.read_csv(self.resultfile, header=None)
        print(df)

    def generate_trim_string(self, trims):
        with open(self.trimfile, "r") as f:
            trims = np.array(f.readlines()[0].split(','))
        ts = "" 
        for r in range(4): 
            for c in range(4): 
                ts += "pixe {0} {1} {2}\n".format(COL_OFFSET + c, ROW_OFFSET + r, trims[4*r + c]) 
        return ts

    def adapt_and_run_script(self, to_adapt, settings):
        with open(to_adapt) as f:
            script = ''.join(line for line in f)
        for setting in settings:
            script = script.replace("#" + setting[0] + "#", str(setting[1]))
        adapted = to_adapt.replace(".roc", "adapted.roc")
        with open(adapted, "w") as f:
            f.write(script)
        os.system('cat {0} | mypsi46test'.format(adapted))

if __name__ == '__main__':
    import sys
    parser =  argparse.ArgumentParser(description='pyxar_mini_cmd',  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-t', '--trim', type=str, help='path to trim file', default='params.trim')
    parser.add_argument('-d', '--dac', type=str, help='path to dac file', default='params.dac')
    parser.add_argument('-r', '--result', type=str, help='path to result file', default='threshold_scan.csv')
    args = parser.parse_args()
    run = pyxar_mini_cmd(args)
    run.threshold_scan()
