# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pxar_mini.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("VP Silicon Experiment")
        MainWindow.resize(800, 720)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 20, 771, 671))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.horizontalLayout2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout2.setObjectName("horizontalLayout2")
        self.horizontalLayout3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout3.setObjectName("horizontalLayout3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pixelAliveButton = QtWidgets.QPushButton(self.widget)
        self.pixelAliveButton.setObjectName("pixelAliveButton")
        self.horizontalLayout.addWidget(self.pixelAliveButton)
        self.thrScanButton = QtWidgets.QPushButton(self.widget)
        self.thrScanButton.setObjectName("thrScanButton")
        self.horizontalLayout.addWidget(self.thrScanButton)
        self.trimRoutineButton = QtWidgets.QPushButton(self.widget)
        self.trimRoutineButton.setObjectName("trimRoutine")
        self.horizontalLayout.addWidget(self.trimRoutineButton)
        self.trimProgressButton = QtWidgets.QPushButton(self.widget)
        self.trimProgressButton.setObjectName("trimProgress")
        self.horizontalLayout.addWidget(self.trimRoutineButton)
        # self.readDacButton = QtWidgets.QPushButton(self.widget)
        # self.readDacButton.setObjectName("readDac")
        # self.horizontalLayout.addWidget(self.readDacButton)
        # self.readTrimButton = QtWidgets.QPushButton(self.widget)
        # self.readTrimButton.setObjectName("readTrim")
        # self.horizontalLayout.addWidget(self.readTrimButton)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout2.addItem(spacerItem2)
        self.colLineEditlabel = QtWidgets.QLabel(self.widget)
        self.colLineEditlabel.setText("Col: ")
        self.horizontalLayout2.addWidget(self.colLineEditlabel)
        self.colLineEdit = QtWidgets.QLineEdit(self.widget)
        self.colLineEdit.setObjectName("colText")
        self.colLineEdit.setMaximumWidth(50)
        self.horizontalLayout2.addWidget(self.colLineEdit)
        self.rowLineEditlabel = QtWidgets.QLabel(self.widget)
        self.rowLineEditlabel.setText("Row: ")
        self.horizontalLayout2.addWidget(self.rowLineEditlabel)
        self.rowLineEdit = QtWidgets.QLineEdit(self.widget)
        self.rowLineEdit.setObjectName("rowText")
        self.rowLineEdit.setMaximumWidth(50)
        self.horizontalLayout2.addWidget(self.rowLineEdit)
        self.phScanButton = QtWidgets.QPushButton(self.widget)
        self.phScanButton.setObjectName("phScan")
        self.horizontalLayout2.addWidget(self.phScanButton)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout2.addItem(spacerItem3)

        self.vthrcompLineEditlabel = QtWidgets.QLabel(self.widget)
        self.vthrcompLineEditlabel.setText("Vthrcomp: ")
        self.vthrcompLineEdit = QtWidgets.QLineEdit(self.widget)
        self.vthrcompLineEdit.setObjectName("vthrcompText")
        self.vthrcompLineEdit.setMaximumWidth(50)

        self.setDacButton = QtWidgets.QPushButton(self.widget)
        self.setDacButton.setObjectName("setDacScan")
        self.resetDacButton = QtWidgets.QPushButton(self.widget)
        self.resetDacButton.setObjectName("resetDacScan")

        self.vtrimLineEditlabel = QtWidgets.QLabel(self.widget)
        self.vtrimLineEditlabel.setText("Vtrim: ")
        self.vtrimLineEdit = QtWidgets.QLineEdit(self.widget)
        self.vtrimLineEdit.setObjectName("vtrimText")
        self.vtrimLineEdit.setMaximumWidth(50)
        
        self.horizontalLayout3.addItem(spacerItem4)
        self.horizontalLayout3.addWidget(self.vthrcompLineEditlabel)
        self.horizontalLayout3.addWidget(self.vthrcompLineEdit)
        self.horizontalLayout3.addWidget(self.vtrimLineEditlabel)
        self.horizontalLayout3.addWidget(self.vtrimLineEdit)
        self.horizontalLayout3.addWidget(self.setDacButton)
        self.horizontalLayout3.addWidget(self.resetDacButton)
        self.horizontalLayout3.addItem(spacerItem4)

        self.verticalLayout.addLayout(self.horizontalLayout)
        self.verticalLayout.addLayout(self.horizontalLayout2)
        self.verticalLayout.addLayout(self.horizontalLayout3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 25))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("VP Silicon Experiment", "VP Silicon Experiment"))
        self.pixelAliveButton.setText(_translate("VP Silicon Experiment", "Pixel Alive"))
        self.trimRoutineButton.setText(_translate("VP Silicon Experiment", "Trim Routine"))
        self.trimProgressButton.setText(_translate("VP Silicon Experiment", "Trim Progress"))
        self.thrScanButton.setText(_translate("VP Silicon Experiment", "Threshold Scan"))
        # self.readDacButton.setText(_translate("VP Silicon Experiment", "Read DAC File"))
        # self.readTrimButton.setText(_translate("VP Silicon Experiment", "Read Trim File"))
        self.phScanButton.setText(_translate("VP Silicon Experiment", "Pulseheight Scan"))
        self.setDacButton.setText(_translate("VP Silicon Experiment", "Set DACs"))
        self.resetDacButton.setText(_translate("VP Silicon Experiment", "Reset DACs to default"))


