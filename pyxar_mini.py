# import logging
from ui_pyxar_mini_mod import Ui_MainWindow
import numpy as np
import pandas as pd
import os
from time import sleep
from matplotlib.ticker import MaxNLocator

#Ui_MainWindow, QMainWindow = loadUiType('spectrum_analyzer.ui')
# Ui_MainWindow, QMainWindow = uic.loadUi('spectrum_analyzer.ui')
# log = logging.getLogger()
# DEBUG = False

from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox
from PyQt5 import QtGui, QtCore, QtWidgets, uic
from matplotlib.pyplot import Figure
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas, NavigationToolbar2QT
plt.style.use('ggplot')
cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']


COL_OFFSET = 20
ROW_OFFSET = 35
VTRIM_DEFAULT = 45
VTHRCOMP_DEFAULT = 67
TRIM_TARGET = 80

class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        # self.ui = uic.loadUi("pyxar_mini.ui", self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self._init_mpl()
        self.channels = {}
        self.summaries = {}
        self.combinations = {}
        self._connect_buttons()
        self.cb = None
        self.thresholds = []
        self.threshold_means = []
        self.threshold_stds = []
        self.trims = []
        vtrim, vthrcomp = self.read_dacs()
        self.ui.vtrimLineEdit.setText(str(vtrim))
        self.ui.vthrcompLineEdit.setText(str(vthrcomp))

    def _connect_buttons(self):
        self.ui.pixelAliveButton.clicked.connect(self.on_pixel_alive)
        self.ui.thrScanButton.clicked.connect(self.on_thresholdscan)
        # self.ui.readDacButton.clicked.connect(self.on_read_dac_file)
        # self.ui.readTrimButton.clicked.connect(self.on_read_trim_file)
        self.ui.phScanButton.clicked.connect(self.on_phscan)
        self.ui.resetDacButton.clicked.connect(self.on_resetDac)
        self.ui.setDacButton.clicked.connect(self.on_setDac)
        self.ui.trimRoutineButton.clicked.connect(self.on_trim_routine)
        self.ui.trimProgressButton.clicked.connect(self.on_trim_progress)

    def on_trim_routine(self):

        #remove this line when implementing your algorithm
        return 

        self.reset_trim() # set all pixels to untrimmed
        self.on_resetDac()
        self.threshold_means = []
        self.threshold_stds = []
        vtrim_initial, vthrcomp_initial = self.read_dacs() # store initial value of DACs

        self.on_thresholdscan() # perform initial scan
        # the thresholds are now saved in the list self.thresholds

        # update the vtrim and vthrcomp DAC
        self.write_dacs(vtrim, vthrcomp)

        # Do this after every optimization step to get a progress in the end
        self.threshold_means.append(np.mean(self.thresholds))
        self.threshold_stds.append(np.std(self.thresholds))


        print(self.threshold_means)
        print(self.threshold_stds)
        print(self.trims)
        print("Set vtrim to: {0}".format(vtrim))
        print("Set vthrcomp to: {0}".format(vthrcomp))
        return
        


    def on_resetDac(self):
        self.write_dacs(VTRIM_DEFAULT, VTHRCOMP_DEFAULT)

    def on_setDac(self):
        self.write_dacs(self.ui.vtrimLineEdit.text(), self.ui.vthrcompLineEdit.text())
        

    def update_text_edits(self):
        vtrim, vthrcomp = self.read_dacs()
        self.ui.vtrimLineEdit.setText(str(vtrim))
        self.ui.vthrcompLineEdit.setText(str(vthrcomp))


    def on_read_dac_file(self):
        fname = QFileDialog.getOpenFileName(self, 'Open DAC file', os.path.basename(__file__), "Data Files (*.dat)")[0]
        print(fname)
        with open(fname, "r") as f:
            dac_settings = f.readlines()
        with open("params.dac", "w") as f:
            f.write("".join(dac_settings))

    def on_read_trim_file(self):
        fname = QFileDialog.getOpenFileName(self, 'Open Trim file', os.path.basename(__file__), "Data Files (*.dat)")[0]
        print(fname)
        with open(fname, "r") as f:
            trim_settings = f.readlines()
        with open("params.trim", "w") as f:
            f.write("".join(trim_settings))




    def on_trim_progress(self):
        iterations = list(range(len(self.threshold_stds)))
        self.ax_clear()
        self.ax.set_xlabel(r'Iterations')
        self.ax.set_ylabel(r'Threshold Standard Deviations')
        self.ax.set_title(r'Trimming Progress')
        line1 = self.ax.plot(iterations, self.threshold_stds, cycle[0], label="Std Dev")
        self.ax2 = self.ax.twinx()
        self.ax2.grid(False)
        self.ax2.set_ylabel(r'Threshold Mean')
        line2 = self.ax2.plot(iterations, self.threshold_means, cycle[1], label="Mean")
        # line3 = self.ax2.plot(iterations, [TRIM_TARGET]*len(iterations), 'k--', label="Target")
        # line3 = self.ax2.hlines(TRIM_TARGET, xmin=0, xmax=len(self.threshold_stds)-1, color='k', label="Target")[0]
        lines = line1 + line2
        labels = [l.get_label() for l in lines]
        self.ax.legend(lines, labels, loc=0)
        # self.ax.yaxis.set_major_locator(plt.MaxNLocator(5))
        self.canvas.draw()

    def read_trim(self):
        with open("params.trim", "r") as f:
            self.trims = np.array([int(t) for t in f.readlines()[0].split(',')])

    def write_trim(self, trims):
        self.trims = trims
        with open("params.trim", "w") as f:
            f.write(",".join([str(i) for i in trims]))

    
    def reset_trim(self):
        self.write_trim(np.full(16,15))

    def generate_trim_string(self):
        self.read_trim()
        ts = "" 
        for r in range(4): 
            for c in range(4): 
                ts += "pixe {0} {1} {2}\n".format(COL_OFFSET + c, ROW_OFFSET + r, self.trims[4*c + r]) 
        return ts
    
    def ax_clear(self):
        self.ax.clear()
        try:
            self.cb.remove()
        except:
            pass
        try:
            self.ax2.clear()
        except:
            pass

    def read_dacs(self):
        with open("params.dac", "r") as f:
            lines = f.readlines()
        vtrim = [int(l.split()[2]) for l in lines if 'vtrim' in l]
        vthrcomp = [int(l.split()[2]) for l in lines if 'vthrcomp' in l]
        return vtrim[0], vthrcomp[0]

    def write_dacs(self, vtrim, vthrcomp):
        with open("params.dac", "r") as f:
            lines = f.readlines()
        for n, l in enumerate(lines):
            if 'vtrim' in l:
                lines[n] = " 11 vtrim      {0}\n".format(vtrim)
        for n, l in enumerate(lines):
            if 'vthrcomp' in l:
                lines[n] = " 12 vthrcomp   {0}\n".format(vthrcomp)
        with open("params.dac", "w") as f:
            f.write("".join(lines))
        self.update_text_edits()
        
    def read_coords(self):
        try:
            row = int(self.ui.rowLineEdit.text())
            col = int(self.ui.colLineEdit.text())
            if row > 3 or row < 0 or col < 0 or col > 3:
                raise ValueError
            return row,col
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Invalid Coordinates")
            msg.setWindowTitle("Invalid Coordinates")
            msg.setInformativeText("Both Row and Col must be between 0 and 3!")
            msg.exec_()
            return

    def on_phscan(self):

        self.ax_clear()
        self.ax.set_xlabel(r'Vcal')
        self.ax.set_ylabel(r'Pulseheight')
        self.ax.set_title(r'Pulseheight Scan')

        row, col = self.read_coords()
        data_dir = os.path.abspath("data")
        csv = os.path.join(data_dir, "phscan.csv")
        replacement_settings = [("col", col), 
                                ("row", row),
                                ("outfile", csv),
                                ("dacparam", os.path.abspath("params.dac")),
                                ("coloff", COL_OFFSET),
                                ("rowoff", ROW_OFFSET),
                                ("trim", self.generate_trim_string())]
        self.adapt_and_run_script("roc_scripts/phscan.roc", replacement_settings)
        with open(csv, "r") as f:
            phs = f.read()
        phs = np.array(phs.split(","))
        vcals = range(256)
        self.ax.plot(vcals[:-1], phs[:-1])
        self.ax.yaxis.set_major_locator(plt.MaxNLocator(5))
        self.canvas.draw()

    def adapt_and_run_script(self, to_adapt, settings):
        with open(to_adapt) as f:
            script = ''.join(line for line in f)
        for setting in settings:
            script = script.replace("#" + setting[0] + "#", str(setting[1]))
        adapted = to_adapt.replace(".roc", "adapted.roc")
        with open(adapted, "w") as f:
            f.write(script)
        os.system('cat {0} | mypsi46test'.format(adapted))


    def on_pixel_alive(self):
        self.ax.clear()
        data_dir = os.path.abspath("data")
        csv = os.path.join(data_dir, "pixelalive.csv")
        replacement_settings = [("outfile", csv),
                                ("dacparam", os.path.abspath("params.dac")),
                                ("coloff", COL_OFFSET),
                                ("rowoff", ROW_OFFSET),
                                ("trim", self.generate_trim_string())]
        self.adapt_and_run_script("roc_scripts/pixelalive.roc", replacement_settings)
        df = pd.read_csv(csv, header=None)
        self.plot_color_map(df, r'Pixel Alive', 9, 11)

    def on_thresholdscan(self):
        self.ax.clear()
        data_dir = os.path.abspath("data")
        csv = os.path.join(data_dir, "threshold_scan.csv")
        replacement_settings = [("dacparam", os.path.abspath("params.dac")),
                                ("coloff", COL_OFFSET),
                                ("rowoff", ROW_OFFSET),
                                ("outfile", csv),
                                ("trim", self.generate_trim_string())]
        self.adapt_and_run_script("roc_scripts/thrmap.roc", replacement_settings)
        df = pd.read_csv(csv, header=None)
        self.thresholds = df.values.flatten()
        print(self.thresholds)
        self.plot_color_map(df, r"Threshold Scan", 70, 90)
        for (i, j), z in np.ndenumerate(df):
            self.ax.text(j, i, '{:0.1f}'.format(z), ha='center', va='center', bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))
        self.canvas.draw()
        self.canvas.flush_events()

    def plot_color_map(self, data, title, vmin, vmax):
        self.ax.clear()
        try:
            self.cb.remove()
        except:
            pass
        self.ax.set_title(title)
        self.ax.set_xlabel(r'Columns')
        self.ax.set_ylabel(r'Rows')
        self.ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        self.ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        x = np.arange(-0.5, 4, 1)  # len = 11
        y = np.arange(-0.5, 4, 1)
        c = self.ax.pcolormesh(x,y, data, edgecolors='k', linewidths=4, vmin=vmin, vmax=vmax)
        self.cb = self.fig.colorbar(c, ax = self.ax)
        self.canvas.draw()
        self.canvas.flush_events()

    def _init_mpl(self):
        scale = 3.213
        self.mplwindow = self.ui.widget
        self.fig = Figure(dpi=130)
        # self.fig = Figure(figsize=(scale*1.85, scale*2.5), dpi=130)
        self.ax = self.fig.add_subplot(111)  # row 0, co13l 0
        # self.fig.tight_layout()
        self.canvas = FigureCanvas(self.fig)
        self.ui.verticalLayout.addWidget(self.canvas)
        self.toolbar = NavigationToolbar2QT(self.canvas, self.ui.widget, coordinates=True)
        self.ui.verticalLayout.addWidget(self.toolbar)

if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication
    

    app = QApplication(sys.argv)
    main = Main()
    main.show()
    sys.exit(app.exec_())

